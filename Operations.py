from pymongo import MongoClient
from bson.objectid import ObjectId
# connect to MongoDB, change the << MONGODB URL >> to reflect your own connection string
#client = MongoClient('mongodb+srv://guna:Guna@studentcluster.lnjf9.mongodb.net/myFirstDatabase?retryWrites=true&w=majority')

client = MongoClient('mongodb://127.0.0.1:27017/')

lis= client['studentdb']
studentCollection = lis['student']
# Retrieve all students present in the database
def return_student(student):
    return {
        'studentid' : str(student['_id']),
        'name' : student['name'],
        'dob' : student['dob'],
        'clas' : student['clas'],
        'section' : student['section'],
        "clsTeacherName" : student["clsTeacherName"],
        'email' : student['email']} 

#   retrieve single student from the database
async def retrieve_student(id : str):
    student = studentCollection.find_one({'_id' :ObjectId(id)})
    if student:
        return return_student(student)
    else:
        return "student id not found"


# return all the students from the database
async def retrieve_students():
    students = []
    for student in studentCollection.find({}):
        students.append(return_student(student))
    return students

# add a single student in the database
async def add_student(student : dict) -> dict:
    student =  studentCollection.insert_one(student)
    new_student = studentCollection.find_one({'_id': student.inserted_id})
    return return_student(new_student)


#delete a single student in a database
async def delete_student_document(student_id : str ) :
    student = studentCollection.find_one({'_id' : ObjectId(student_id)})
    if student :
        studentCollection.delete_one({'_id' :  ObjectId(student_id)})
        return {"Deleted_student": student_id}
    
    else:
        return "student id not found"


# update a student details in a database
async def update_student(id : str, data : dict) -> dict:
    student = studentCollection.find_one({ '_id' : ObjectId(id)})
    if student:
        updated_student = studentCollection.update_one( {'_id' : ObjectId(id)}, { '$set' : data})
        print(updated_student)
        if updated_student:
            return return_student(studentCollection.find_one({'_id' : ObjectId(id)}))
        else:
            return False
        
        
        
        
