from fastapi import FastAPI
from pydantic import BaseModel
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse
from Operations import *

app = FastAPI()


class Student(BaseModel):
    name: str
    dob: str
    clas: str
    section: str
    clsTeacherName: str
    email: str


@app.put('/student/')
async def create_student(student:Student )  -> dict:
    stu_json = jsonable_encoder(student)
    student = await add_student(stu_json)    
    return 'Student successfully added',student


@app.put('/students/')
async def create_students(students: list):
    student_documents = []
    for student in students:
        stu_json = jsonable_encoder(student)
        student_documents.append(await add_student(stu_json))
    return student_documents

        
@app.get('/{id}/')
async def get_student(id: str):
    student = await retrieve_student(id)
    return student


@app.get('/')
async def get_students():
    students = await retrieve_students()
    return students


@app.delete('/{delete_id}/')
async def delete_student(delete_id:str) -> str:
    deleted_student = await delete_student_document(delete_id)
    return deleted_student


@app.put("/{stu_id}/")
async def update_students(stu_id : str , data: dict) -> dict:
    student = await update_student(stu_id, data)
    if student == False:
        return "Student not updated"
    else:
        return "updated successfully",student

